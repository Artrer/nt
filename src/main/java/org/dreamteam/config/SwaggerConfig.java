package org.dreamteam.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Tag;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        ApiInfo apiInfo = new ApiInfo("","","", null, new Contact("", "", ""), null, null, new ArrayList<>());
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo)
                .tags(new Tag("34","33"))
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.dreamteam"))
                .paths(PathSelectors.any())
                .build();
    }
}
