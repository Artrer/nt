package org.dreamteam.domain;

import com.sun.istack.internal.NotNull;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@NoArgsConstructor
@ApiModel("TestResult")
@Table(name = "test_result")
public class TestResult {
    /**
     * Идентификатор
     */
    @ApiModelProperty("Id")
    @Id
    @Column(name = "test_result_id")
    private Long Id;

    /**
     * Имя операции
     */
    @ApiModelProperty("operationName")
    @Length(min = 1, max = 255)
    @NotNull
    @Column(name = "test_result_operation_name")
    private String operationName;

    /**
     * Minimum
     */
    @ApiModelProperty("testResultMin")
    @Range(min = 1, max = 15)
    @Column(name = "test_result_min")
    private Float testResultMin;

    /**
     * Average
     */
    @ApiModelProperty("testResultAverage")
    @Range(min = 1, max = 15)
    @Column(name = "test_result_average")
    private Float testResultAverage;

    /**
     * Max
     */
    @ApiModelProperty("testResultMax")
    @Range(min = 1, max = 15)
    @Column(name = "test_result_max")
    private Float testResultMax;

    /**
     * Pass
     */
    @ApiModelProperty("testResultPass")
    @Range(min = 1, max = 15)
    @Column(name = "test_result_pass")
    private Float testResultPass;

}
