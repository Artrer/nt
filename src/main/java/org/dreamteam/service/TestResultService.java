package org.dreamteam.service;


import lombok.RequiredArgsConstructor;
import org.dreamteam.domain.TestResult;
import org.dreamteam.repository.TestResultRepo;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
@RequiredArgsConstructor
public class TestResultService {

    /**
     * Репозиторий для работы со студентами
     */
    private final TestResultRepo testResultRepo;

    public TestResult saveOrUpdate(TestResult testResult) {
        return testResultRepo.save(testResult);
    }

    public void deleteById(Long id) {
        testResultRepo.deleteById(id);
    }

    public List<TestResult> getAll() {
        return testResultRepo.findAll();
    }

    public TestResult getById(Long id) {
        return testResultRepo.findById(id).orElse(null);
    }
}
