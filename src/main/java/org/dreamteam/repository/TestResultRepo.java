package org.dreamteam.repository;

import org.dreamteam.domain.TestResult;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TestResultRepo extends JpaRepository<TestResult, Long> {

}
