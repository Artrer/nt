package org.dreamteam.controller.api;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.dreamteam.domain.TestResult;
import org.dreamteam.service.TestResultService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/test-result")
@Slf4j
@RequiredArgsConstructor
@Api(value = "TestResultController")
public class TestResultController {
    /**
     * Сервис для работы с студентами
     */
    private final TestResultService testResultService;


    @PutMapping
    @ApiOperation("сохранение студента1")
    public ResponseEntity<TestResult> saveOrUpdate(@ApiParam(value = "testResult") @RequestBody @Valid TestResult testResult) {
        return ResponseEntity.ok(testResultService.saveOrUpdate(testResult));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("сохранение студента2")
    public ResponseEntity deleteStudentById(@PathVariable Long id) {
        testResultService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/all")
    @ApiOperation("сохранение студента3")
    public List<TestResult> getAllStudents() {
        return testResultService.getAll();
    }

    @GetMapping("/{id}")
    @ApiOperation("сохранение студента4")
    public ResponseEntity<TestResult> getStudentById(@PathVariable Long id) {
        return ResponseEntity.ok(testResultService.getById(id));
    }

    @ExceptionHandler
    public ResponseEntity exceptionHandler(Exception e) {
        log.error("TestResult Controller error", e);
        return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
