package org.dreamteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestLoaderApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestLoaderApplication.class);
    }
}
